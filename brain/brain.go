package brain

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/gonum/floats"
	"gitlab.com/i-tre-brutti/common/interfaces"
)

// Zappa take the project status and elaborate statistics on it
func Zappa(statuses interfaces.ProjectStatus) interfaces.ProjectStatistics {
	var results interfaces.ProjectStatistics
	results.ProjectID = statuses.ProjectID
	results.CreationDate = statuses.Src.Code.FirstCommit
	results.Age = int64(time.Since(statuses.Src.Code.FirstCommit).Hours() / 24)
	results.DocumentVersion = statuses.DocumentVersion
	for _, cpm := range statuses.Src.Code.CommitsPerMonth {
		results.Activity += cpm
	}
	results.Expansion = calcExpansionPercentage(statuses.Src.Code.TotalCodeSize)
	offset := calcSizeRanges(statuses.Src.Code.CodeSizePerFile)
	results.CodeRelativeDiffusion = calcRelativeDiffusion(statuses.Src.Code.CodeSizePerFile, offset)
	results.CodeAbsoluteDiffusion = calcAbsoluteDiffusion(statuses.Src.Code.CodeSizePerFile)

	// TODO: remove random numbers
	results.Trend = 0 // type of trend, crescent decrescent gaussian
	results.Popularity = calcPopularity(statuses.Src.Social)
	results.Quality = calcQuality(statuses.Issue)
	results.Contributors = calcContributors(statuses.Src.Social.Contributors)
	results.ActivityDistribution = calcActivityDistribution(statuses.Src.Code.CommitsPerMonth)
	return results
}

/*
We calculate popularity giving value to each attr.
Contributors represent 70% of the quality, Stars 20% and forks 10%. We currently ignore watchers.
*/
func calcPopularity(social interfaces.SrcSocial) float64 {
	// Numbers after which we consider 100% quality for each category
	maxForks := 50.
	maxStars := 100.

	ret := 0.

	ret += calcContributors(social.Contributors) * 0.7

	if social.Forks > 0 {
		ret += (float64(social.Forks) / maxForks) * 0.1
	}

	if social.Stars > 0 {
		ret += (float64(social.Stars) / maxStars) * 0.2
	}
	// social.Watchers (ignored)

	if ret > 1 {
		log.Println("How can it be???")
		ret = 1
	}

	return ret
}

func calcContributors(contributors int) float64 {
	maxContributors := 20.
	ret := 0.
	if contributors > 0 {
		ret += float64(contributors) / maxContributors
	}
	return ret
}

func calcQuality(social interfaces.IssueData) float64 {
	return .35
}

func calcExpansionPercentage(size int) float64 {
	gtkSize := 70000000.0 // ~ the size of gtk repo at 12/2017
	res := floats.Round(float64(size)/gtkSize, 2)
	if res < 0.10 {
		return 0.10
	}
	if res > 1.0 {
		return 1.0
	}
	return res
}

func calcSizeRanges(fileSizes []interfaces.RepoFileSize) int {
	sum := 0
	for _, fs := range fileSizes {
		sum += fs.Size
	}
	avg := sum / len(fileSizes)

	offset := avg / 4
	return offset
}

// CalcRelativeDiffusion return an array of 5 float with every chunks percentage
func calcRelativeDiffusion(fileSizes []interfaces.RepoFileSize, offset int) [5]float64 {
	var res [5]float64
	fileCounter := 0
	for _, fs := range fileSizes {
		size := fs.Size
		fileCounter++
		switch {
		case size <= offset*1:
			res[0]++
		case size <= offset*2:
			res[1]++
		case size <= offset*3:
			res[2]++
		case size <= offset*4:
			res[3]++
		case size > offset*4:
			res[4]++
		}
	}
	// change numbers to percentage
	for i := 0; i <= 4; i++ {
		perc := res[i] / float64(fileCounter)
		res[i] = floats.Round(perc, 2)
	}
	return res
}

func calcAbsoluteDiffusion(fileSizes []interfaces.RepoFileSize) [5]float64 {
	var res [5]float64
	offset := 1000000 / 5 // from gtk biggest file of 998194 bytes
	fileCounter := 0
	for _, fs := range fileSizes {
		size := fs.Size
		fileCounter++
		switch {
		case size <= offset*1:
			res[0]++
		case size <= offset*2:
			res[1]++
		case size <= offset*3:
			res[2]++
		case size <= offset*4:
			res[3]++
		case size > offset*4:
			res[4]++
		}
	}
	// change numbers to percentage
	for i := 0; i <= 4; i++ {
		perc := res[i] / float64(fileCounter)
		res[i] = floats.Round(perc, 2)
	}
	return res
}

/*
 *	Inspect the data to understand the growing-shrinkin pattern.
 *
 *	Idea:
 *	- the current cluster of data is finished when the inspected a sample
 *		is smaller than half the step of of the cluster (from the min value, aka
 *		the start of the cluster, to the max value of the cluster).
 *	- we are entering a new cluster of data when the  inspected sample
 *		is bigger than half the difference bewteen the last cluster's max
 *		value and the successive min value (which will become the new starting
 *		point of the new cluster).
 *
 *	Note: all the "bigger/smaller than half" is just an arbitrary decision,
 *		there is no mathematics fundamentals backing this thing up :(
 */
func _calcStartsOfWorkBursts(commitsPerMonth []int) []int {
	result := make([]int, 0)
	currentMinValue := 0
	currentMinIndex := 0
	currentMaxValue := 0
	threshold := func() int {
		return (currentMaxValue + currentMinValue) / 2
	}
	lookingFor := "MAX"
	for i, v := range commitsPerMonth {
		if lookingFor == "MAX" {
			if v < currentMinValue {
				currentMinValue = v
				currentMinIndex = i
			} else if v > threshold() {
				result = append(result, currentMinIndex)
				currentMaxValue = v
				lookingFor = "MIN"
			}
		} else if lookingFor == "MIN" {
			if v > currentMaxValue {
				currentMaxValue = v
			} else if v < threshold() {
				currentMinValue = v
				currentMinIndex = i
				lookingFor = "MAX"
			}
		}
	}
	return result
}
func calcActivityDistribution(commitsPerMonth map[string]int) []float64 {
	startingAge := "999999"
	for k := range commitsPerMonth {
		if k < startingAge {
			startingAge = k
		}
	}
	startingYear, _ := strconv.ParseInt(startingAge[:4], 10, 0)
	startingMonth, _ := strconv.ParseInt(startingAge[4:], 10, 0)
	// Note: assume there are no "gaps" in the list of commitsPerMonth!
	orderedCommits := make([]int, len(commitsPerMonth))
	commitsSum := 0
	for i := range orderedCommits {
		key := fmt.Sprintf("%04d%02d", startingYear, startingMonth)
		c, ok := commitsPerMonth[key]
		if !ok {
			log.Printf("key '%s' not found in commitsPerMonth", key)
		} else {
			orderedCommits[i] = c
			commitsSum += c
		}
		startingMonth++
		if startingMonth > 12 {
			startingMonth = 1
			startingYear++
		}
	}
	burstsEndsIndexes := _calcStartsOfWorkBursts(orderedCommits)
	result := make([]float64, 0)
	sum := func(elements []int) int {
		result := 0
		for _, v := range elements {
			result += v
		}
		return result
	}
	for i := 1; i < len(burstsEndsIndexes); i++ {
		total := sum(orderedCommits[burstsEndsIndexes[i-1]:burstsEndsIndexes[i]])
		result = append(result, float64(total)/float64(commitsSum))
	}
	total := sum(orderedCommits[burstsEndsIndexes[len(burstsEndsIndexes)-1]:])
	result = append(result, float64(total)/float64(commitsSum))
	return result
}
