package brain

import (
	"math"
	"testing"
)

func TestCalcStartsOfWorkBurst(t *testing.T) {

	tables := []struct {
		datapoints   []int
		burstsStarts []int
	}{
		// growing curve
		{[]int{1, 2, 3, 4, 5, 6}, []int{0}},
		// shrinking curve
		{[]int{6, 5, 4, 3, 2, 1}, []int{0}},
		// 2 simple bursts
		{[]int{1, 10, 1, 9, 1}, []int{0, 2}},
		// 2 complex bursts with
		{[]int{8, 10, 8, 9, 8, 2, 5, 4, 10, 1}, []int{0, 5}},
	}

	for _, table := range tables {
		result := _calcStartsOfWorkBursts(table.datapoints)
		if len(result) != len(table.burstsStarts) {
			t.Errorf("Expected a different number of bursts: got %d (%v), expected %d (%v)",
				len(result), result, len(table.burstsStarts), table.burstsStarts)
		} else {
			for i := range result {
				if result[i] != table.burstsStarts[i] {
					t.Errorf("Expected different bursts starts: got %v, expected %v",
						result, table.burstsStarts)
					break
				}
			}
		}
	}
}

func TestCalcActivityDistribution(t *testing.T) {
	data := map[string]int{
		"200108": 1,
		"200109": 10,
		"200110": 1,
		"200111": 10,
	}
	expected := []float64{.5, .5}

	result := calcActivityDistribution(data)
	if len(expected) != len(result) {
		t.Errorf("Expected a different amount of elements: got %d (%v), expected %d (%v)",
			len(result), result, len(expected), expected)
	} else {
		for i := range result {
			if math.Abs(result[i]-expected[i]) > 1e-6 {
				t.Errorf("Expected a different result: got %v, expected %v",
					result, expected)
			}
		}
	}
}
