package main

import (
	"log"
	"sync"

	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/smandrupper/brain"
	"gopkg.in/mgo.v2/bson"
)

func main() {
	// Manage database connection
	db.InitSession()
	defer db.CloseSession()

	ch := make(chan interfaces.ProjectStatistics)
	statuses := getProjectsStatuses()

	var waitGroup sync.WaitGroup
	waitGroup.Add(len(statuses))

	for _, s := range statuses {
		log.Println("Spawning smandrupper for project", s.ProjectID)
		go func(c interfaces.ProjectStatus) {
			defer waitGroup.Done()
			ch <- brain.Zappa(c)
		}(s)
	}

	var wc sync.WaitGroup
	wc.Add(len(statuses))
	go func() {
		for result := range ch {
			defer wc.Done()
			saveProjectStatistics(result)
		}
	}()

	waitGroup.Wait()
	close(ch)
	wc.Wait()
}

func saveProjectStatistics(stats interfaces.ProjectStatistics) {
	collection := db.GetStatisticsCollection()

	var currentStats interfaces.ProjectStatistics
	// Find the current statistics of the project in the db, if it exists
	err := collection.Find(bson.M{"project_id": stats.ProjectID, "document_version": stats.DocumentVersion}).One(&currentStats)

	if err != nil {
		// The statistic doesn't exist, create it
		if err.Error() == "not found" {
			log.Println("Creating new statistic for project", stats.ProjectID)
			insertError := collection.Insert(stats)
			if insertError != nil {
				panic(insertError)
			}
			return
		}
		panic(err)
	}

	// The status exists, update it
	log.Println("Updating statistics for project", stats.ProjectID)
	updateErr := collection.Update(bson.M{"project_id": currentStats.ProjectID, "document_version": stats.DocumentVersion}, stats)
	if updateErr != nil {
		panic(updateErr)
	}

}

func getProjectsStatuses() []interfaces.ProjectStatus {
	var result []interfaces.ProjectStatus

	err := db.GetStatusCollection().Find(bson.M{}).All(&result)
	if err != nil {
		panic(err)
	}
	return result
}
